# DMMN-SDCM

A Tensorflow implementation for DMMN-SDCM

## Quick Start

- Create three empty folders: 'analysis' for saving analyzing results and 'logs' for saving experiment logs
- Download the 300-dimensional pre-trained word vectors from [Glove](https://nlp.stanford.edu/projects/glove/) and save it in the 'data' folder as 'data/glove.840B.300d.txt'
- If you want to see the performance of the trained model, you should uncomment some codes in the `model.py`

## Source Code Tree

```
|--- data

|	|--- laptop

|	|--- restaurant

|--- main.py

|--- model.py

|--- utils.py

|--- README.md
```

## Settings

T 2
L 4
R 5
